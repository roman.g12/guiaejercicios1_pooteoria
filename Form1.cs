﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuiaEjercicios_Ejercicio1
{
    public partial class Form1 : Form
    {
        //Listas con la que llenaremos el dataGrid
        private List<clsEmpleado> Empleado = new List<clsEmpleado>();
        private List<clsIngenieria> Estudiante = new List<clsIngenieria>();
        //Instancias de las clases
        clsEmpleado empleado = new clsEmpleado();
        clsIngenieria estu = new clsIngenieria();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRegistrarEmpleado_Click(object sender, EventArgs e)
        {
            //Invocamos el método que se encargara de validar que los campos no queden vacíos
            string mensaje = empleado.validarVacios(txtDui.Text, txtNombre.Text, txtDireccion.Text, txtNombreEmpresa.Text, txtSalario.Text, dtpFechaNacimiento.Value);

            if (mensaje == "Datos Correctos")
            {
                try
                {
                    //Invocar método que registrara a los Empleados y los guardara en una lista
                    Empleado = empleado.registrarEmpleado(txtDui.Text, txtNombre.Text, txtDireccion.Text, txtNombreEmpresa.Text, double.Parse(txtSalario.Text), dtpFechaNacimiento.Value);

                    dgvDatosEmpleados.DataSource = null;
                    dgvDatosEmpleados.DataSource = Empleado; /*los nombres de columna que veremos son los de las propiedades*/

                    //Orientamos las columas
                    dgvDatosEmpleados.Columns["Nombre"].DisplayIndex = 0;
                    dgvDatosEmpleados.Columns["Dui"].DisplayIndex = 1;
                    dgvDatosEmpleados.Columns["Salario"].DisplayIndex = 2;
                    dgvDatosEmpleados.Columns["Nombreempresa"].DisplayIndex = 3;
                    dgvDatosEmpleados.Columns["Fechanac"].DisplayIndex = 4;
                    dgvDatosEmpleados.Columns["Direccasa"].DisplayIndex = 5;
                    dgvDatosEmpleados.Columns["Edad_persona"].DisplayIndex = 6;
                    //Cambiamos los headers de algunas columnas
                    dgvDatosEmpleados.Columns["Nombreempresa"].HeaderText = "Nombre empresa";
                    dgvDatosEmpleados.Columns["Edad_persona"].HeaderText = "Edad";
                    //No mostramos los campos de fecha nacimiento y dirección
                    dgvDatosEmpleados.Columns["Fechanac"].Visible = false;
                    dgvDatosEmpleados.Columns["Direccasa"].Visible = false;

                    txtNombre.Clear();
                    txtDui.Clear();
                    txtDireccion.Clear();
                    txtSalario.Clear();
                    txtNombreEmpresa.Clear();
                }
                catch (Exception ex)
                {
                    string mensajeError = empleado.mensajeErrorNumeroString(true); //Invocamos al método que contine nuestro mensaje de error
                    MessageBox.Show(mensajeError, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(mensaje, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnRegistrarEstudiante_Click(object sender, EventArgs e)
        {
            //Invocamos el método que se encargara de validar que los campos no queden vacíos
            string mensaje = estu.validarVaciosEstudiante(txtDuiEstudiante.Text, txtNombreEstudiante.Text, txtDireccionEstudiante.Text, txtCarnet.Text, txtCarrera.Text, dtpFechaNacimientoEstudiante.Value, txtProeycto.Text, txtHorasPasantias.Text);

            if (mensaje == "Datos Correctos")
            {
                try
                {
                    //Invocar método que registrara a los estudiantes y los guardara en una lista
                    Estudiante = estu.registrarEstudiante(txtDuiEstudiante.Text, txtNombreEstudiante.Text, txtDireccionEstudiante.Text, txtCarnet.Text, txtCarrera.Text, Int32.Parse(txtHorasPasantias.Text), txtProeycto.Text, dtpFechaNacimientoEstudiante.Value);

                    dgvDatosE.DataSource = null;
                    dgvDatosE.DataSource = Estudiante; /*los nombres de columna que veremos son los de las propiedades*/

                    //Orientamos las columas
                    dgvDatosE.Columns["Nombre"].DisplayIndex = 0;
                    dgvDatosE.Columns["Dui"].DisplayIndex = 1;
                    dgvDatosE.Columns["Carnet"].DisplayIndex = 2;
                    dgvDatosE.Columns["Carrera"].DisplayIndex = 3;
                    dgvDatosE.Columns["Nombreproyecto"].DisplayIndex = 4;
                    dgvDatosE.Columns["Totalhoraspasantia"].DisplayIndex = 5;
                    dgvDatosE.Columns["Direccasa"].DisplayIndex = 6;
                    dgvDatosE.Columns["Edad_persona"].DisplayIndex = 7;
                    //Cambiamos los headers de algunas columnas
                    dgvDatosE.Columns["Nombreproyecto"].HeaderText = "Nombre proyecto";
                    dgvDatosE.Columns["Totalhoraspasantia"].HeaderText = "Total Pasantías";
                    dgvDatosE.Columns["Edad_persona"].HeaderText = "Edad";
                    //No mostramos los campos de fecha nacimiento y dirección
                    dgvDatosE.Columns["Fechanac"].Visible = false;
                    dgvDatosE.Columns["Direccasa"].Visible = false;

                    txtNombreEstudiante.Clear();
                    txtDuiEstudiante.Clear();
                    txtDireccionEstudiante.Clear();
                    txtCarrera.Clear();
                    txtCarnet.Clear();
                    txtProeycto.Clear();
                    txtHorasPasantias.Clear();
                }
                catch(Exception ex)
                {
                    string mensajeError = empleado.mensajeErrorNumeroString(false); //Invocamos al método que contine nuestro mensaje de error
                    MessageBox.Show(mensajeError, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(mensaje, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
