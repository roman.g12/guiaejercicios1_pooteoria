﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuiaEjercicios_Ejercicio1
{
    class clsEstudiante : clsPersona
    {
        private string carnet;
        private string carrera;

        public clsEstudiante()
        {
            carnet = "pendiente";
            carrera = "sin definir";
        }

        public clsEstudiante(string carnet, string carrera)
        {
            this.carnet = carnet;
            this.carrera = carrera;
        }

        public string Carnet { get => carnet; set => carnet = value; }
        public string Carrera { get => carrera; set => carrera = value; }
    }
}
