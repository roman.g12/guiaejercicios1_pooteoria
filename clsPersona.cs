﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace GuiaEjercicios_Ejercicio1
{
    class clsPersona //Clase base
    {
        private List<clsEmpleado> Personas = new List<clsEmpleado>();
        private List<clsIngenieria> Estudi = new List<clsIngenieria>();

        protected string dui;
        protected string nombre;
        protected DateTime fechanac;
        protected string direccasa;
        protected int edad_persona;

        public clsPersona()
        {
            dui = "Sin asignar";
            nombre = "vacio";
            fechanac = DateTime.Parse("01-01-2000");
            direccasa = "pendiente";
            edad_persona = 0;
        }

        public clsPersona(string dui, string nombre, DateTime fechanac, string direccasa)
        {
            this.dui = dui;
            this.nombre = nombre;
            this.fechanac = fechanac;
            this.direccasa = direccasa;
        }

        //Método para calcular la edad del usuario
        public int Edad(DateTime nacimiento)
        {
            //DateTime nacimiento = new DateTime(2000, 1, 25); //Fecha de nacimiento
            int edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;

            return edad;
        }

        public string validarVacios(string dui, string nombre, string direccion, string empresa, string salario, DateTime nacimiento)
        {
            string mensaje;
            
            if (dui != "" && nombre != "" && direccion != "" && empresa != "" && salario != "")
            {
                if (nacimiento.Year >= 2003) //Si el estudiante es menor de edad
                {
                    mensaje = "No es posible registrar empleados menores a 18 años";
                }
                else
                {
                    //Los campos no quedaron vacíos
                    mensaje = "Datos Correctos";
                }
            }
            else
            {
                mensaje = "No dejar campos vacíos";
            }

            return mensaje;
        }

        public string validarVaciosEstudiante(string dui, string nombre, string direccion, string carnet, string carrerra, DateTime nacimiento, string proyecto, string horasPasantias)
        {
            string mensaje;

            if (dui != "" && nombre != "" && direccion != "" && carnet != "" && carrerra != "" && proyecto != "" && horasPasantias != "")
            {
                if (nacimiento.Year >= 2018) //Si el usuario tiene menos de 3 años
                {
                    mensaje = "No es posible registrar menores de 3 años";
                }
                else
                {
                    //Los campos no quedaron vacíos
                    mensaje = "Datos Correctos";
                }
            }
            else
            {
                mensaje = "No dejar campos vacíos";
            }

            return mensaje;
        }

        public string mensajeErrorNumeroString(bool EmpleadoEstudiante)
        {
            string mensaje;

            if (EmpleadoEstudiante == true)
            {
                mensaje = "ERROR!! El campo de salario solo debe ser números";
            }
            else
            {
                mensaje = "ERROR!! El campo de Horas de pasantía solo debe ser números";

            }
            
            return mensaje;
        }

        public List<clsEmpleado> registrarEmpleado(string dui, string nombre, string direccion, string empresa, double salario, DateTime fechaNacimiento)
        {
            clsEmpleado empleado = new clsEmpleado();
            int edad;

            //Agregamos los datos de la clase
            empleado.Nombre = nombre;
            empleado.Dui = dui;
            empleado.Direccasa = direccion;
            empleado.Nombreempresa = empresa;
            empleado.Salario = salario;
            empleado.Fechanac = fechaNacimiento;

            //Calculamos la edad
            edad = Edad(fechaNacimiento);

            empleado.Edad_persona = edad;

            Personas.Add(empleado); //Registramos en nuestra lista al empleado

            return Personas; //Retornamos el list para actualizar el DataGrid
        }

        public List<clsIngenieria> registrarEstudiante(string duiE, string nombreE, string direccionE, string carnetE, string carreraE, int horasE, string proyectoE, DateTime fechaNacimientoE)
        {
            clsIngenieria estudiante = new clsIngenieria();
            int edad;

            estudiante.Nombre = nombreE;
            estudiante.Dui = duiE;
            estudiante.Direccasa = direccionE;
            estudiante.Carnet = carnetE;
            estudiante.Carrera = carreraE;
            estudiante.Fechanac = fechaNacimientoE;
            estudiante.Totalhoraspasantia = horasE;
            estudiante.Nombreproyecto = proyectoE;

            //Calculamos la edad
            edad = Edad(fechaNacimientoE);

            estudiante.Edad_persona = edad;

            Estudi.Add(estudiante); //Registramos en nuestra lista al estudiante

            return Estudi; //Retornamos el list para actualizar el DataGrid
        }

        public string Dui { get => dui; set => dui = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public DateTime Fechanac { get => fechanac; set => fechanac = value; }
        public string Direccasa { get => direccasa; set => direccasa = value; }
        public int Edad_persona { get => edad_persona; set => edad_persona = value; }
    }
}
