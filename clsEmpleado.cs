﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuiaEjercicios_Ejercicio1
{
    class clsEmpleado : clsPersona
    {
        private string nombreempresa;
        private double salario;

        public clsEmpleado()
        {
            nombreempresa = "Sin definir";
            salario = 50;
        }

        public clsEmpleado(string nombreempresa, double salario)
        {
            this.nombreempresa = nombreempresa;
            this.salario = salario;
        }

        public string Nombreempresa { get => nombreempresa; set => nombreempresa = value; }
        public double Salario { get => salario; set => salario = value; }
    }
}
