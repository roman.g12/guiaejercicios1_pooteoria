﻿
namespace GuiaEjercicios_Ejercicio1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbcRegistros = new System.Windows.Forms.TabControl();
            this.tbpEmpleados = new System.Windows.Forms.TabPage();
            this.btnRegistrarEmpleado = new System.Windows.Forms.Button();
            this.txtSalario = new System.Windows.Forms.TextBox();
            this.lblSalario = new System.Windows.Forms.Label();
            this.txtNombreEmpresa = new System.Windows.Forms.TextBox();
            this.lblNombreEmpresa = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtDui = new System.Windows.Forms.TextBox();
            this.lblDUI = new System.Windows.Forms.Label();
            this.dgvDatosEmpleados = new System.Windows.Forms.DataGridView();
            this.tbpEstudiantes = new System.Windows.Forms.TabPage();
            this.txtHorasPasantias = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProeycto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRegistrarEstudiante = new System.Windows.Forms.Button();
            this.txtCarrera = new System.Windows.Forms.TextBox();
            this.lblCarrera = new System.Windows.Forms.Label();
            this.txtCarnet = new System.Windows.Forms.TextBox();
            this.lblCarnet = new System.Windows.Forms.Label();
            this.txtDireccionEstudiante = new System.Windows.Forms.TextBox();
            this.lblDireccionEstudiante = new System.Windows.Forms.Label();
            this.dtpFechaNacimientoEstudiante = new System.Windows.Forms.DateTimePicker();
            this.lblFechaNacimientoEstudiante = new System.Windows.Forms.Label();
            this.txtNombreEstudiante = new System.Windows.Forms.TextBox();
            this.lblNombreEstudiante = new System.Windows.Forms.Label();
            this.txtDuiEstudiante = new System.Windows.Forms.TextBox();
            this.lblDuiEstudiante = new System.Windows.Forms.Label();
            this.dgvDatosE = new System.Windows.Forms.DataGridView();
            this.tbcRegistros.SuspendLayout();
            this.tbpEmpleados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosEmpleados)).BeginInit();
            this.tbpEstudiantes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosE)).BeginInit();
            this.SuspendLayout();
            // 
            // tbcRegistros
            // 
            this.tbcRegistros.Controls.Add(this.tbpEmpleados);
            this.tbcRegistros.Controls.Add(this.tbpEstudiantes);
            this.tbcRegistros.Location = new System.Drawing.Point(12, 12);
            this.tbcRegistros.Name = "tbcRegistros";
            this.tbcRegistros.SelectedIndex = 0;
            this.tbcRegistros.Size = new System.Drawing.Size(800, 483);
            this.tbcRegistros.TabIndex = 0;
            // 
            // tbpEmpleados
            // 
            this.tbpEmpleados.Controls.Add(this.btnRegistrarEmpleado);
            this.tbpEmpleados.Controls.Add(this.txtSalario);
            this.tbpEmpleados.Controls.Add(this.lblSalario);
            this.tbpEmpleados.Controls.Add(this.txtNombreEmpresa);
            this.tbpEmpleados.Controls.Add(this.lblNombreEmpresa);
            this.tbpEmpleados.Controls.Add(this.txtDireccion);
            this.tbpEmpleados.Controls.Add(this.lblDireccion);
            this.tbpEmpleados.Controls.Add(this.dtpFechaNacimiento);
            this.tbpEmpleados.Controls.Add(this.lblFechaNacimiento);
            this.tbpEmpleados.Controls.Add(this.txtNombre);
            this.tbpEmpleados.Controls.Add(this.lblNombre);
            this.tbpEmpleados.Controls.Add(this.txtDui);
            this.tbpEmpleados.Controls.Add(this.lblDUI);
            this.tbpEmpleados.Controls.Add(this.dgvDatosEmpleados);
            this.tbpEmpleados.Location = new System.Drawing.Point(4, 24);
            this.tbpEmpleados.Name = "tbpEmpleados";
            this.tbpEmpleados.Padding = new System.Windows.Forms.Padding(3);
            this.tbpEmpleados.Size = new System.Drawing.Size(792, 455);
            this.tbpEmpleados.TabIndex = 0;
            this.tbpEmpleados.Text = "Registrar empleados";
            this.tbpEmpleados.UseVisualStyleBackColor = true;
            // 
            // btnRegistrarEmpleado
            // 
            this.btnRegistrarEmpleado.Location = new System.Drawing.Point(671, 173);
            this.btnRegistrarEmpleado.Name = "btnRegistrarEmpleado";
            this.btnRegistrarEmpleado.Size = new System.Drawing.Size(101, 42);
            this.btnRegistrarEmpleado.TabIndex = 13;
            this.btnRegistrarEmpleado.Text = "Registrar Empleado";
            this.btnRegistrarEmpleado.UseVisualStyleBackColor = true;
            this.btnRegistrarEmpleado.Click += new System.EventHandler(this.btnRegistrarEmpleado_Click);
            // 
            // txtSalario
            // 
            this.txtSalario.Location = new System.Drawing.Point(561, 76);
            this.txtSalario.Name = "txtSalario";
            this.txtSalario.Size = new System.Drawing.Size(100, 23);
            this.txtSalario.TabIndex = 12;
            // 
            // lblSalario
            // 
            this.lblSalario.AutoSize = true;
            this.lblSalario.Location = new System.Drawing.Point(413, 79);
            this.lblSalario.Name = "lblSalario";
            this.lblSalario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblSalario.Size = new System.Drawing.Size(45, 15);
            this.lblSalario.TabIndex = 11;
            this.lblSalario.Text = "Salario:";
            // 
            // txtNombreEmpresa
            // 
            this.txtNombreEmpresa.Location = new System.Drawing.Point(561, 22);
            this.txtNombreEmpresa.Name = "txtNombreEmpresa";
            this.txtNombreEmpresa.Size = new System.Drawing.Size(200, 23);
            this.txtNombreEmpresa.TabIndex = 10;
            // 
            // lblNombreEmpresa
            // 
            this.lblNombreEmpresa.AutoSize = true;
            this.lblNombreEmpresa.Location = new System.Drawing.Point(413, 25);
            this.lblNombreEmpresa.Name = "lblNombreEmpresa";
            this.lblNombreEmpresa.Size = new System.Drawing.Size(130, 15);
            this.lblNombreEmpresa.TabIndex = 9;
            this.lblNombreEmpresa.Text = "Nombre de la empresa:";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(145, 192);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(200, 23);
            this.txtDireccion.TabIndex = 8;
            // 
            // lblDireccion
            // 
            this.lblDireccion.AutoSize = true;
            this.lblDireccion.Location = new System.Drawing.Point(18, 192);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(60, 15);
            this.lblDireccion.TabIndex = 7;
            this.lblDireccion.Text = "Dirección:";
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(145, 130);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(136, 23);
            this.dtpFechaNacimiento.TabIndex = 6;
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(18, 136);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(120, 15);
            this.lblFechaNacimiento.TabIndex = 5;
            this.lblFechaNacimiento.Text = "Fecha de nacimineto:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(145, 76);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(200, 23);
            this.txtNombre.TabIndex = 4;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(18, 79);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(54, 15);
            this.lblNombre.TabIndex = 3;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtDui
            // 
            this.txtDui.Location = new System.Drawing.Point(145, 22);
            this.txtDui.Name = "txtDui";
            this.txtDui.Size = new System.Drawing.Size(200, 23);
            this.txtDui.TabIndex = 2;
            // 
            // lblDUI
            // 
            this.lblDUI.AutoSize = true;
            this.lblDUI.Location = new System.Drawing.Point(18, 25);
            this.lblDUI.Name = "lblDUI";
            this.lblDUI.Size = new System.Drawing.Size(29, 15);
            this.lblDUI.TabIndex = 1;
            this.lblDUI.Text = "DUI:";
            // 
            // dgvDatosEmpleados
            // 
            this.dgvDatosEmpleados.AllowUserToAddRows = false;
            this.dgvDatosEmpleados.AllowUserToDeleteRows = false;
            this.dgvDatosEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosEmpleados.Location = new System.Drawing.Point(125, 239);
            this.dgvDatosEmpleados.Name = "dgvDatosEmpleados";
            this.dgvDatosEmpleados.ReadOnly = true;
            this.dgvDatosEmpleados.RowTemplate.Height = 25;
            this.dgvDatosEmpleados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDatosEmpleados.Size = new System.Drawing.Size(555, 210);
            this.dgvDatosEmpleados.TabIndex = 0;
            // 
            // tbpEstudiantes
            // 
            this.tbpEstudiantes.Controls.Add(this.dgvDatosE);
            this.tbpEstudiantes.Controls.Add(this.txtHorasPasantias);
            this.tbpEstudiantes.Controls.Add(this.label2);
            this.tbpEstudiantes.Controls.Add(this.txtProeycto);
            this.tbpEstudiantes.Controls.Add(this.label1);
            this.tbpEstudiantes.Controls.Add(this.btnRegistrarEstudiante);
            this.tbpEstudiantes.Controls.Add(this.txtCarrera);
            this.tbpEstudiantes.Controls.Add(this.lblCarrera);
            this.tbpEstudiantes.Controls.Add(this.txtCarnet);
            this.tbpEstudiantes.Controls.Add(this.lblCarnet);
            this.tbpEstudiantes.Controls.Add(this.txtDireccionEstudiante);
            this.tbpEstudiantes.Controls.Add(this.lblDireccionEstudiante);
            this.tbpEstudiantes.Controls.Add(this.dtpFechaNacimientoEstudiante);
            this.tbpEstudiantes.Controls.Add(this.lblFechaNacimientoEstudiante);
            this.tbpEstudiantes.Controls.Add(this.txtNombreEstudiante);
            this.tbpEstudiantes.Controls.Add(this.lblNombreEstudiante);
            this.tbpEstudiantes.Controls.Add(this.txtDuiEstudiante);
            this.tbpEstudiantes.Controls.Add(this.lblDuiEstudiante);
            this.tbpEstudiantes.Location = new System.Drawing.Point(4, 24);
            this.tbpEstudiantes.Name = "tbpEstudiantes";
            this.tbpEstudiantes.Padding = new System.Windows.Forms.Padding(3);
            this.tbpEstudiantes.Size = new System.Drawing.Size(792, 455);
            this.tbpEstudiantes.TabIndex = 1;
            this.tbpEstudiantes.Text = "Resgistrar estudiantes";
            this.tbpEstudiantes.UseVisualStyleBackColor = true;
            // 
            // txtHorasPasantias
            // 
            this.txtHorasPasantias.Location = new System.Drawing.Point(519, 196);
            this.txtHorasPasantias.Name = "txtHorasPasantias";
            this.txtHorasPasantias.Size = new System.Drawing.Size(160, 23);
            this.txtHorasPasantias.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(386, 202);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 15);
            this.label2.TabIndex = 25;
            this.label2.Text = "Total hora de pasantías";
            // 
            // txtProeycto
            // 
            this.txtProeycto.Location = new System.Drawing.Point(519, 143);
            this.txtProeycto.Name = "txtProeycto";
            this.txtProeycto.Size = new System.Drawing.Size(160, 23);
            this.txtProeycto.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(386, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 15);
            this.label1.TabIndex = 23;
            this.label1.Text = "Proyecto:";
            // 
            // btnRegistrarEstudiante
            // 
            this.btnRegistrarEstudiante.Location = new System.Drawing.Point(674, 248);
            this.btnRegistrarEstudiante.Name = "btnRegistrarEstudiante";
            this.btnRegistrarEstudiante.Size = new System.Drawing.Size(101, 42);
            this.btnRegistrarEstudiante.TabIndex = 22;
            this.btnRegistrarEstudiante.Text = "Registrar Estudiante";
            this.btnRegistrarEstudiante.UseVisualStyleBackColor = true;
            this.btnRegistrarEstudiante.Click += new System.EventHandler(this.btnRegistrarEstudiante_Click);
            // 
            // txtCarrera
            // 
            this.txtCarrera.Location = new System.Drawing.Point(519, 86);
            this.txtCarrera.Name = "txtCarrera";
            this.txtCarrera.Size = new System.Drawing.Size(160, 23);
            this.txtCarrera.TabIndex = 20;
            // 
            // lblCarrera
            // 
            this.lblCarrera.AutoSize = true;
            this.lblCarrera.Location = new System.Drawing.Point(386, 89);
            this.lblCarrera.Name = "lblCarrera";
            this.lblCarrera.Size = new System.Drawing.Size(48, 15);
            this.lblCarrera.TabIndex = 19;
            this.lblCarrera.Text = "Carrera:";
            // 
            // txtCarnet
            // 
            this.txtCarnet.Location = new System.Drawing.Point(519, 29);
            this.txtCarnet.Name = "txtCarnet";
            this.txtCarnet.Size = new System.Drawing.Size(160, 23);
            this.txtCarnet.TabIndex = 18;
            // 
            // lblCarnet
            // 
            this.lblCarnet.AutoSize = true;
            this.lblCarnet.Location = new System.Drawing.Point(386, 32);
            this.lblCarnet.Name = "lblCarnet";
            this.lblCarnet.Size = new System.Drawing.Size(45, 15);
            this.lblCarnet.TabIndex = 17;
            this.lblCarnet.Text = "Carnet:";
            // 
            // txtDireccionEstudiante
            // 
            this.txtDireccionEstudiante.Location = new System.Drawing.Point(150, 199);
            this.txtDireccionEstudiante.Name = "txtDireccionEstudiante";
            this.txtDireccionEstudiante.Size = new System.Drawing.Size(200, 23);
            this.txtDireccionEstudiante.TabIndex = 16;
            // 
            // lblDireccionEstudiante
            // 
            this.lblDireccionEstudiante.AutoSize = true;
            this.lblDireccionEstudiante.Location = new System.Drawing.Point(23, 199);
            this.lblDireccionEstudiante.Name = "lblDireccionEstudiante";
            this.lblDireccionEstudiante.Size = new System.Drawing.Size(60, 15);
            this.lblDireccionEstudiante.TabIndex = 15;
            this.lblDireccionEstudiante.Text = "Dirección:";
            // 
            // dtpFechaNacimientoEstudiante
            // 
            this.dtpFechaNacimientoEstudiante.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimientoEstudiante.Location = new System.Drawing.Point(150, 137);
            this.dtpFechaNacimientoEstudiante.Name = "dtpFechaNacimientoEstudiante";
            this.dtpFechaNacimientoEstudiante.Size = new System.Drawing.Size(136, 23);
            this.dtpFechaNacimientoEstudiante.TabIndex = 14;
            // 
            // lblFechaNacimientoEstudiante
            // 
            this.lblFechaNacimientoEstudiante.AutoSize = true;
            this.lblFechaNacimientoEstudiante.Location = new System.Drawing.Point(23, 143);
            this.lblFechaNacimientoEstudiante.Name = "lblFechaNacimientoEstudiante";
            this.lblFechaNacimientoEstudiante.Size = new System.Drawing.Size(120, 15);
            this.lblFechaNacimientoEstudiante.TabIndex = 13;
            this.lblFechaNacimientoEstudiante.Text = "Fecha de nacimineto:";
            // 
            // txtNombreEstudiante
            // 
            this.txtNombreEstudiante.Location = new System.Drawing.Point(150, 83);
            this.txtNombreEstudiante.Name = "txtNombreEstudiante";
            this.txtNombreEstudiante.Size = new System.Drawing.Size(200, 23);
            this.txtNombreEstudiante.TabIndex = 12;
            // 
            // lblNombreEstudiante
            // 
            this.lblNombreEstudiante.AutoSize = true;
            this.lblNombreEstudiante.Location = new System.Drawing.Point(23, 86);
            this.lblNombreEstudiante.Name = "lblNombreEstudiante";
            this.lblNombreEstudiante.Size = new System.Drawing.Size(54, 15);
            this.lblNombreEstudiante.TabIndex = 11;
            this.lblNombreEstudiante.Text = "Nombre:";
            // 
            // txtDuiEstudiante
            // 
            this.txtDuiEstudiante.Location = new System.Drawing.Point(150, 29);
            this.txtDuiEstudiante.Name = "txtDuiEstudiante";
            this.txtDuiEstudiante.Size = new System.Drawing.Size(200, 23);
            this.txtDuiEstudiante.TabIndex = 10;
            // 
            // lblDuiEstudiante
            // 
            this.lblDuiEstudiante.AutoSize = true;
            this.lblDuiEstudiante.Location = new System.Drawing.Point(23, 32);
            this.lblDuiEstudiante.Name = "lblDuiEstudiante";
            this.lblDuiEstudiante.Size = new System.Drawing.Size(29, 15);
            this.lblDuiEstudiante.TabIndex = 9;
            this.lblDuiEstudiante.Text = "DUI:";
            // 
            // dgvDatosE
            // 
            this.dgvDatosE.AllowUserToAddRows = false;
            this.dgvDatosE.AllowUserToDeleteRows = false;
            this.dgvDatosE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosE.Location = new System.Drawing.Point(23, 248);
            this.dgvDatosE.Name = "dgvDatosE";
            this.dgvDatosE.ReadOnly = true;
            this.dgvDatosE.RowTemplate.Height = 25;
            this.dgvDatosE.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDatosE.Size = new System.Drawing.Size(645, 201);
            this.dgvDatosE.TabIndex = 27;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 507);
            this.Controls.Add(this.tbcRegistros);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tbcRegistros.ResumeLayout(false);
            this.tbpEmpleados.ResumeLayout(false);
            this.tbpEmpleados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosEmpleados)).EndInit();
            this.tbpEstudiantes.ResumeLayout(false);
            this.tbpEstudiantes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosE)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbcRegistros;
        private System.Windows.Forms.TabPage tbpEmpleados;
        private System.Windows.Forms.TabPage tbpEstudiantes;
        private System.Windows.Forms.DataGridView dgvDatosEmpleados;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtDui;
        private System.Windows.Forms.Label lblDUI;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label lblDireccion;
        private System.Windows.Forms.TextBox txtSalario;
        private System.Windows.Forms.Label lblSalario;
        private System.Windows.Forms.TextBox txtNombreEmpresa;
        private System.Windows.Forms.Label lblNombreEmpresa;
        private System.Windows.Forms.Button btnRegistrarEmpleado;
        private System.Windows.Forms.TextBox txtDireccionEstudiante;
        private System.Windows.Forms.Label lblDireccionEstudiante;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimientoEstudiante;
        private System.Windows.Forms.Label lblFechaNacimientoEstudiante;
        private System.Windows.Forms.TextBox txtNombreEstudiante;
        private System.Windows.Forms.Label lblNombreEstudiante;
        private System.Windows.Forms.TextBox txtDuiEstudiante;
        private System.Windows.Forms.Label lblDuiEstudiante;
        private System.Windows.Forms.TextBox txtCarrera;
        private System.Windows.Forms.Label lblCarrera;
        private System.Windows.Forms.TextBox txtCarnet;
        private System.Windows.Forms.Label lblCarnet;
        private System.Windows.Forms.Button btnRegistrarEstudiante;
        private System.Windows.Forms.TextBox txtHorasPasantias;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProeycto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvDatosE;
    }
}

