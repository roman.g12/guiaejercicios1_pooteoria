﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GuiaEjercicios_Ejercicio1
{
    class clsIngenieria : clsEstudiante
    {
        private string nombreproyecto;
        private int totalhoraspasantia;

        public clsIngenieria()
        {
            nombreproyecto = "no definido";
            totalhoraspasantia = 0;
        }

        public clsIngenieria(string nombreproyecto, int totalhoraspasantia)
        {
            this.nombreproyecto = nombreproyecto;
            this.totalhoraspasantia = totalhoraspasantia;
        }

        public string Nombreproyecto { get => nombreproyecto; set => nombreproyecto = value; }
        public int Totalhoraspasantia { get => totalhoraspasantia; set => totalhoraspasantia = value; }
    }
}
